/// @file
/// @brief File consists of external functions and helpful data for .c file
#ifndef PESTRUCT_H_
#define PESTRUCT_H_

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/// @brief This enum includes all possible cases which may occur while reading
enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_HEADER_SIGNATURE
};

/// @brief This func writes section of PE file into binary one
/// @param[in] in pointer to input file
/// @param name section label
/// @param[out] out pointer to output file
void write_pe_info(FILE* in, const char* name, FILE* out);

#endif /* PESTRUCT_H_*/
