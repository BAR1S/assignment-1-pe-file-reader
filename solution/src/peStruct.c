/// @file
/// @brief There are struct of PE file and fucntions to work with it
#include "peStruct.h"


/// @brief DOS header of PE file
struct 
#if defined __clang__ || __GNUC__
    __attribute__((packed))
#elif defined _MSC_VER
    #pragma pack(pash, 1)
#endif
DOS_header{
    /// Magic number. Should be 'MZ'
    uint16_t e_magic;
    /// Bytes on last page of file
    uint16_t e_cblp;
    /// Pages in file
    uint16_t e_cp;
    /// Relocations
    uint16_t e_crlc;
    /// Size of header in paragraphs
    uint16_t e_cparhdr;
    /// Minimum extra paragraphs needed
    uint16_t e_minalloc;
    /// Maximum extra paragraphs needed
    uint16_t e_maxalloc;
    /// Initial (relative) SS value
    uint16_t e_ss;
    /// Initial SP value
    uint16_t e_sp;
    /// Checksum
    uint16_t e_csum;
    /// Initial IP value
    uint16_t e_ip;
    /// Initial (relative) CS value
    uint16_t e_cs;
    /// File address of relocation table
    uint16_t e_lfarlc;
    /// Overlay number
    uint16_t e_ovno;
    /// Reserved words
    uint16_t e_res[4];
    /// OEM identifier (for e_oeminfo) 
    uint16_t e_oemid;
    /// OEM information; e_oemid specific
    uint16_t e_oeminfo;
    /// Reserved words
    uint16_t e_res2[10];
    /// File address of new exe header
    uint32_t e_lfanew;
};
#ifdef _MSC_VER
#pragma pack(pop)
#endif

/// @brief Packed DOS header. Includes DOS header and status with which it was read 
struct
#if defined __clang__ || __GNUC__
    __attribute__((packed))
#elif defined _MSC_VER
    #pragma pack(pash, 1)
#endif
packed_DOS_header{
    /// DOS header of PE file
    struct DOS_header dos_hdr;
    /// Status of read information
    enum read_status status;
};
#ifdef _MSC_VER
#pragma pack(pop)
#endif

/// @brief COFF header of PE file
typedef struct 
#if defined __clang__ || __GNUC__
    __attribute__((packed))
#elif defined _MSC_VER
    #pragma pack(pash, 1)
#endif
{
    /// The number that identifies the type of target machine
    uint16_t Machine;
    /// The number of sections. This indicates the size of the section table, which immediately follows the headers
    uint16_t NumberOfSections;
    /// The low 32 bits of the number of seconds since 00:00 January 1, 1970 (a C run-time time_t value), which indicates when the file was created
    uint32_t TimeDateStamp;
    /// The file offset of the COFF symbol table, or zero if no COFF symbol table is present. This value should be zero for an image because COFF debugging information is deprecated
    uint32_t PointerToSymbolTable;
    /// The number of entries in the symbol table. This data can be used to locate the string table, which immediately follows the symbol table. This value should be zero for an image because COFF debugging information is deprecated
    uint32_t NumberOfSymbols;
    /// The size of the optional header, which is required for executable files but not for object files. This value should be zero for an object file
    uint16_t SizeOfOptionalHeader;
    /// The flags that indicate the attributes of the file
    uint16_t Characteristics;
} COFF_file_header;
#ifdef _MSC_VER
#pragma pack(pop)
#endif

/// @brief Structure of data directory. Array od data directories is a part of optional header of PE file
typedef struct 
#if defined __clang__ || __GNUC__
    __attribute__((packed))
#elif defined _MSC_VER
    #pragma pack(pash, 1)
#endif
{
    /// The RVA of the table
    uint32_t VirtualAddress;
    /// Size in bytes
    uint32_t Size;  
} image_data_directory;
#ifdef _MSC_VER
#pragma pack(pop)
#endif

/// @brief Optional header for x64 architecture
typedef struct 
#if defined __clang__ || __GNUC__
    __attribute__((packed))
#elif defined _MSC_VER
    #pragma pack(pash, 1)
#endif
{
    /// The unsigned integer that identifies the state of the image file
    uint16_t Magic;
    /// The linker major version number
    uint8_t MajorLinkerVersion;
    /// The linker minor version number
    uint8_t MinorLinkerVersion;
    /// The size of the code (text) section, or the sum of all code sections if there are multiple sections
    uint32_t SizeOfCode;
    /// The size of the initialized data section, or the sum of all such sections if there are multiple data sections
    uint32_t SizeOfInitializedData;
    /// The size of the uninitialized data section (BSS), or the sum of all such sections if there are multiple BSS sections
    uint32_t SizeOfUninitializedData;
    /// The address of the entry point relative to the image base when the executable file is loaded into memory
    uint32_t AddressOfEntryPoint;
    /// The address that is relative to the image base of the beginning-of-code section when it is loaded into memory
    uint32_t BaseOfCode;
    /// The preferred address of the first byte of image when loaded into memory
    uint64_t ImageBase;
    /// The alignment (in bytes) of sections when they are loaded into memory. It must be greater than or equal to FileAlignment
    uint32_t SectionAlignment;
    /// The alignment factor (in bytes) that is used to align the raw data of sections in the image file
    uint32_t FileAlignment;
    /// The major version number of the required operating system
    uint16_t MajorOperatingSystemVersion;
    /// The minor version number of the required operating system
    uint16_t MinorOperatingSystemVersion;
    /// The major version number of the image
    uint16_t MajorImageVersion;
    /// The minor version number of the image
    uint16_t MinorImageVersion;
    /// The major version number of the subsystem
    uint16_t MajorSubsystemVersion;
    /// The minor version number of the subsystem
    uint16_t MinorSubsystemVersion;
    /// Reserved, must be zero
    uint32_t Win32VersionValue;
    /// The size (in bytes) of the image, including all headers, as the image is loaded in memory. It must be a multiple of SectionAlignment
    uint32_t SizeOfImage;
    /// The combined size of an MS-DOS stub, PE header, and section headers rounded up to a multiple of FileAlignment
    uint32_t SizeOfHeaders;
    /// The image file checksum
    uint32_t CheckSum;
    /// The subsystem that is required to run this image
    uint16_t Subsystem;
    /// Special characteristics for DLL
    uint16_t DllCharacteristics;
    /// The size of the stack to reserve
    uint64_t SizeOfStackReserve;
    /// The size of the stack to commit
    uint64_t SizeOfStackCommit;
    /// The size of the local heap space to reserve
    uint64_t SizeOfHeapReserve;
    /// The size of the local heap space to commit
    uint64_t SizeOfHeapCommit;
    /// Reserved, must be zero
    uint32_t LoaderFlags;
    /// The number of data-directory entries in the remainder of the optional header. Each describes a location and size
    uint32_t NumberOfRvaAndSizes;
    /// Array of data directories
    image_data_directory DataDirectory[16]; 
} Optional_header_32_plus;
#ifdef _MSC_VER
#pragma pack(pop)
#endif

/// @brief Optional header for x32 architecture
typedef struct 
#if defined __clang__ || __GNUC__
    __attribute__((packed))
#elif defined _MSC_VER
    #pragma pack(pash, 1)
#endif
{
    /// The unsigned integer that identifies the state of the image file
    uint16_t Magic;
    /// The linker major version number
    uint8_t MajorLinkerVersion;
    /// The linker minor version number
    uint8_t MinorLinkerVersion;
    /// The size of the code (text) section, or the sum of all code sections if there are multiple sections
    uint32_t SizeOfCode;
    /// The size of the initialized data section, or the sum of all such sections if there are multiple data sections
    uint32_t SizeOfInitializedData;
    /// The size of the uninitialized data section (BSS), or the sum of all such sections if there are multiple BSS sections
    uint32_t SizeOfUninitializedData;
    /// The address of the entry point relative to the image base when the executable file is loaded into memory
    uint32_t AddressOfEntryPoint;
    /// The address that is relative to the image base of the beginning-of-code section when it is loaded into memory
    uint32_t BaseOfCode;
    /// The address that is relative to the image base of the beginning-of-data section when it is loaded into memory
    uint32_t BaseOfData;
    /// The preferred address of the first byte of image when loaded into memory
    uint32_t ImageBase;
    /// The alignment (in bytes) of sections when they are loaded into memory. It must be greater than or equal to FileAlignment
    uint32_t SectionAlignment;
    /// The alignment factor (in bytes) that is used to align the raw data of sections in the image file
    uint32_t FileAlignment;
    /// The major version number of the required operating system
    uint16_t MajorOperatingSystemVersion;
    /// The minor version number of the required operating system
    uint16_t MinorOperatingSystemVersion;
    /// The major version number of the image
    uint16_t MajorImageVersion;
    /// The minor version number of the image
    uint16_t MinorImageVersion;
    /// The major version number of the subsystem
    uint16_t MajorSubsystemVersion;
    /// The minor version number of the subsystem
    uint16_t MinorSubsystemVersion;
    /// Reserved, must be zero
    uint32_t Win32VersionValue;
    /// The size (in bytes) of the image, including all headers, as the image is loaded in memory. It must be a multiple of SectionAlignment
    uint32_t SizeOfImage;
    /// The combined size of an MS-DOS stub, PE header, and section headers rounded up to a multiple of FileAlignment
    uint32_t SizeOfHeaders;
    /// The image file checksum
    uint32_t CheckSum;
    /// The subsystem that is required to run this image
    uint16_t Subsystem;
    /// Special characteristics for DLL
    uint16_t DllCharacteristics;
    /// The size of the stack to reserve
    uint32_t SizeOfStackReserve;
    /// The size of the stack to commit
    uint32_t SizeOfStackCommit;
    /// The size of the local heap space to reserve
    uint32_t SizeOfHeapReserve;
    /// The size of the local heap space to commit
    uint32_t SizeOfHeapCommit;
    /// Reserved, must be zero
    uint32_t LoaderFlags;
    /// The number of data-directory entries in the remainder of the optional header. Each describes a location and size
    uint32_t NumberOfRvaAndSizes;
    /// Array of data directories
    image_data_directory DataDirectory[16];
} Optional_header_32;
#ifdef _MSC_VER
#pragma pack(pop)
#endif

/// @brief Optional header for both x32 and x64 architectures
typedef union 
#if defined __clang__ || __GNUC__
    __attribute__((packed))
#elif defined _MSC_VER
    #pragma pack(pash, 1)
#endif
{
    /// Optional header for x32 architecture
    Optional_header_32 opt_header_32;
    /// Optional header for x64 architecture
    Optional_header_32_plus opt_header_32_plus;
} Optional_header;
#ifdef _MSC_VER
#pragma pack(pop)
#endif

/// @brief PE header of PE file
typedef struct
#if defined __clang__ || __GNUC__
    __attribute__((packed))
#elif defined _MSC_VER
    #pragma pack(pash, 1)
#endif
{
    /// PE file signature. Should be 'PE'
    uint32_t Signature;
    /// COFF header of PE file
    COFF_file_header coff_header;
    /// Optional header of PE file
    Optional_header optional_header;
} PE_header;
#ifdef _MSC_VER
#pragma pack(pop)
#endif

/// @brief Packed PE header which additionally includes the status with which it was read
typedef struct
#if defined __clang__ || __GNUC__
    __attribute__((packed))
#elif defined _MSC_VER
    #pragma pack(pash, 1)
#endif
{
    /// PE header
    PE_header pe_hdr;
    /// Status of read information
    enum read_status read_st;
} packed_PE_header;
#ifdef _MSC_VER
#pragma pack(pop)
#endif

/// @brief Section header for PE file 
typedef struct 
#if defined __clang__ || __GNUC__
    __attribute__((packed))
#elif defined _MSC_VER
    #pragma pack(pash, 1)
#endif
{
    /// An 8-byte, null-padded UTF-8 encoded string which represents the name of section
    char Name[8];
    /// The total size of the section when loaded into memory
    uint32_t VirtualSize;
    /// For executable images, the address of the first byte of the section relative to the image base when the section is loaded into memory. For object files, this field is the address of the first byte before relocation is applied
    uint32_t VirtualAddress;
    /// The size of the section (for object files) or the size of the initialized data on disk (for image files)
    uint32_t SizeOfRawData;
    /// The file pointer to the first page of the section within the COFF file
    uint32_t PointerToRawData;
    /// The file pointer to the beginning of relocation entries for the section. This is set to zero for executable images or if there are no relocations
    uint32_t PointerToRelocations;
    /// The file pointer to the beginning of line-number entries for the section. This is set to zero if there are no COFF line numbers. This value should be zero for an image because COFF debugging information is deprecated
    uint32_t PointerToLinenumbers;
    /// The number of relocation entries for the section. This is set to zero for executable images
    uint16_t NumberOfRelocations;
    /// The number of line-number entries for the section. This value should be zero for an image because COFF debugging information is deprecated.
    uint16_t NumberOfLinenumbers;
    /// The flags that describe the characteristics of the section
    uint32_t Characteristics;
} Section_header;
#ifdef _MSC_VER
#pragma pack(pop)
#endif

static uint32_t little_to_big_endian(const uint32_t val) {
    return ((val & 0xff000000) >> 24) |
           ((val & 0x00ff0000) >> 8) |
           ((val & 0x0000ff00) << 8) |
           ((val & 0x000000ff) << 24);
}

/// @brief Reads DOS header of given PE file
/// @param[in] in pointer to input file
/// @return packed_DOS_header
static struct packed_DOS_header reading_dos(FILE* in){
    struct packed_DOS_header dos_hdr = {0};
    const uint64_t read_hdr_bytes = fread(&dos_hdr.dos_hdr, sizeof(dos_hdr.dos_hdr), 1, in);
    if(read_hdr_bytes == sizeof(dos_hdr.dos_hdr)){
        dos_hdr.status = READ_OK;
    }
    else if(little_to_big_endian(dos_hdr.dos_hdr.e_magic) != 0x4D5A0000){
        dos_hdr.status = READ_INVALID_HEADER;
    }
    return dos_hdr;
}

/// @brief Reads DOS program and PE header from given PE file
/// @param[in] in pointer to input file
/// @return packed_PE_header
static packed_PE_header reading_pe(FILE* in){
    const struct packed_DOS_header dos_hdr = reading_dos(in);
    char dos_prog[dos_hdr.dos_hdr.e_lfanew - sizeof(dos_hdr.dos_hdr)];
    fread(&dos_prog, dos_hdr.dos_hdr.e_lfanew - sizeof(dos_hdr.dos_hdr), 1, in);
    packed_PE_header pe_header = {0};
    if(dos_hdr.status == READ_OK){
        const uint64_t read_pe_sign = fread(&pe_header.pe_hdr.Signature, sizeof(pe_header.pe_hdr.Signature), 1, in);
        if((little_to_big_endian(pe_header.pe_hdr.Signature) != 0x50450000) && (read_pe_sign == sizeof(pe_header.pe_hdr.Signature))){
            pe_header.read_st = READ_INVALID_SIGNATURE;
            return pe_header;
        }
        else{
            const uint64_t read_coff_hdr = fread(&pe_header.pe_hdr.coff_header, sizeof(char), sizeof(pe_header.pe_hdr.coff_header), in);
            if(read_coff_hdr != sizeof(pe_header.pe_hdr.coff_header)){
                pe_header.read_st = READ_INVALID_BITS;
                return pe_header;
            }
            else{
                const uint64_t read_opt_hdr = fread(&pe_header.pe_hdr.optional_header, sizeof(char), sizeof(pe_header.pe_hdr.optional_header), in);
                if(read_opt_hdr != sizeof(pe_header.pe_hdr.optional_header)){
                    pe_header.read_st = READ_INVALID_BITS;
                    return pe_header;
                }
                else{
                    pe_header.read_st = READ_OK;
                    return pe_header;
                }
            }
        }
    }
    else{
        pe_header.read_st = dos_hdr.status;
        return pe_header;
    }
}

/// @brief Tries to find section with given name in PE file and writes it to given file
/// @param header header of input PE file  
/// @param name section label
/// @param[in] in pointer to input file
/// @param[out] out pointer to output file
static void find_and_write_section(const packed_PE_header header, const char *name, FILE* in, FILE* out){
    const uint16_t number_of_sections = header.pe_hdr.coff_header.NumberOfSections;
    if(number_of_sections != 0){
        Section_header *sections_hdr = malloc(sizeof(Section_header)*number_of_sections);
        for(size_t i = 0; i < number_of_sections; i++){
            fread(&sections_hdr[i], sizeof(Section_header), 1, in);
            if(strcmp(sections_hdr[i].Name, name) == 0){
                fseek(in, sections_hdr[i].PointerToRawData, SEEK_SET);
                char *section = malloc(sizeof(char)*sections_hdr[i].SizeOfRawData);
                fread(section, sizeof(char)*sections_hdr[i].SizeOfRawData, 1, in);
                fwrite(section, sizeof(char)*sections_hdr[i].SizeOfRawData, 1, out);
                free(sections_hdr); free(section);
                break;
            }
        }
    }
}

void write_pe_info(FILE* in, const char* name, FILE* out){
    packed_PE_header header = reading_pe(in);
    find_and_write_section(header, name, in, out);
}
