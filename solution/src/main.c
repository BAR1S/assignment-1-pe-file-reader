/// @file 
/// @brief Main application file

#include "peStruct.h"
#include <stdio.h>

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f)
{
  fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv)
{
  (void) argc; (void) argv; // supress 'unused parameters' warning
  
  if(argc < 3){
    usage(stderr);
    return 2;  
  }
  else{
    FILE* in; FILE* out;
    if((in = fopen(argv[1], "rb")) == NULL){
      fprintf(stderr, "<in_file> doesn't exist");
      return 3;
    }
    if((out = fopen(argv[3], "wb")) == NULL){
      fprintf(stderr, "Can't open or create <out_file>");
      return 3;
    }
    char *name = argv[2];
    write_pe_info(in, name, out);
    fclose(in); fclose(out);
  }

  return 0;
}

