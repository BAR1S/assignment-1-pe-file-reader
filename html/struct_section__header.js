var struct_section__header =
[
    [ "Characteristics", "struct_section__header.html#a2db2d9b50cd35cc06d223fe021daaf04", null ],
    [ "Name", "struct_section__header.html#a62193ee98afbd65f1c352d1cbc902029", null ],
    [ "NumberOfLinenumbers", "struct_section__header.html#a5311c79a9a446153cf8cf1fd48486fc0", null ],
    [ "NumberOfRelocations", "struct_section__header.html#a14662eb7233065c3c749703cbb494c19", null ],
    [ "PointerToLinenumbers", "struct_section__header.html#a1dc28d60e90f1bda1fc8527134025ac6", null ],
    [ "PointerToRawData", "struct_section__header.html#a5a81028801ea48dab69972b726977fa8", null ],
    [ "PointerToRelocations", "struct_section__header.html#af5ce14e48baf5656f890dfb7c3f488a9", null ],
    [ "SizeOfRawData", "struct_section__header.html#a7489ede56b9c87ae753ef500cc27ad0b", null ],
    [ "VirtualAddress", "struct_section__header.html#a9654185e0d962a473a9b46e4c0fe1dcb", null ],
    [ "VirtualSize", "struct_section__header.html#af8ead30a6531d2a00b7e1e04beec0eba", null ]
];