var pe_struct_8c =
[
    [ "DOS_header", "struct_d_o_s__header.html", "struct_d_o_s__header" ],
    [ "packed_DOS_header", "structpacked___d_o_s__header.html", "structpacked___d_o_s__header" ],
    [ "COFF_file_header", "struct_c_o_f_f__file__header.html", "struct_c_o_f_f__file__header" ],
    [ "image_data_directory", "structimage__data__directory.html", "structimage__data__directory" ],
    [ "Optional_header_32_plus", "struct_optional__header__32__plus.html", "struct_optional__header__32__plus" ],
    [ "Optional_header_32", "struct_optional__header__32.html", "struct_optional__header__32" ],
    [ "Optional_header", "union_optional__header.html", "union_optional__header" ],
    [ "PE_header", "struct_p_e__header.html", "struct_p_e__header" ],
    [ "packed_PE_header", "structpacked___p_e__header.html", "structpacked___p_e__header" ],
    [ "Section_header", "struct_section__header.html", "struct_section__header" ],
    [ "find_and_write_section", "pe_struct_8c.html#a0aa35274d2cb9bda85303d285f152cdc", null ],
    [ "reading_dos", "pe_struct_8c.html#abc9152cc080790e045a44bd17e48267f", null ],
    [ "reading_pe", "pe_struct_8c.html#a9d5c8928e344b7c7bebcdb790d8ba544", null ],
    [ "write_pe_info", "pe_struct_8c.html#a50d5f4ce64c3a313840c168c7d65f5fa", null ]
];