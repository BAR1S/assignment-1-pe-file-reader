var pe_struct_8h =
[
    [ "read_status", "pe_struct_8h.html#a4d84c66cd3c22eb9c38633d500a86303", [
      [ "READ_OK", "pe_struct_8h.html#a4d84c66cd3c22eb9c38633d500a86303a0967ee309ffe95c5ee502fb9c273e30c", null ],
      [ "READ_INVALID_SIGNATURE", "pe_struct_8h.html#a4d84c66cd3c22eb9c38633d500a86303a329a4d4cee2401eebef72e5deca62bd2", null ],
      [ "READ_INVALID_BITS", "pe_struct_8h.html#a4d84c66cd3c22eb9c38633d500a86303a544724d659e2d35b1d1718b6e163214d", null ],
      [ "READ_INVALID_HEADER", "pe_struct_8h.html#a4d84c66cd3c22eb9c38633d500a86303a19ca260284dfe7a9d8371c9b9185d13b", null ],
      [ "READ_INVALID_HEADER_SIGNATURE", "pe_struct_8h.html#a4d84c66cd3c22eb9c38633d500a86303a19449f3290313b61133810a12806cb68", null ]
    ] ],
    [ "write_pe_info", "pe_struct_8h.html#a50d5f4ce64c3a313840c168c7d65f5fa", null ]
];