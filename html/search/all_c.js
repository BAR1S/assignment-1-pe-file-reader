var searchData=
[
  ['packed_5fdos_5fheader_0',['packed_DOS_header',['../structpacked___d_o_s__header.html',1,'']]],
  ['packed_5fpe_5fheader_1',['packed_PE_header',['../structpacked___p_e__header.html',1,'']]],
  ['pe_5fhdr_2',['pe_hdr',['../structpacked___p_e__header.html#ace69d33e9be327bebcec2d58d248d5c6',1,'packed_PE_header']]],
  ['pe_5fheader_3',['PE_header',['../struct_p_e__header.html',1,'']]],
  ['pestruct_2ec_4',['peStruct.c',['../pe_struct_8c.html',1,'']]],
  ['pestruct_2eh_5',['peStruct.h',['../pe_struct_8h.html',1,'']]],
  ['pointertolinenumbers_6',['PointerToLinenumbers',['../struct_section__header.html#a1dc28d60e90f1bda1fc8527134025ac6',1,'Section_header']]],
  ['pointertorawdata_7',['PointerToRawData',['../struct_section__header.html#a5a81028801ea48dab69972b726977fa8',1,'Section_header']]],
  ['pointertorelocations_8',['PointerToRelocations',['../struct_section__header.html#af5ce14e48baf5656f890dfb7c3f488a9',1,'Section_header']]],
  ['pointertosymboltable_9',['PointerToSymbolTable',['../struct_c_o_f_f__file__header.html#aeba29bda97bbb865445ff626d74130a2',1,'COFF_file_header']]]
];
