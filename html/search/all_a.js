var searchData=
[
  ['name_0',['Name',['../struct_section__header.html#a62193ee98afbd65f1c352d1cbc902029',1,'Section_header']]],
  ['numberoflinenumbers_1',['NumberOfLinenumbers',['../struct_section__header.html#a5311c79a9a446153cf8cf1fd48486fc0',1,'Section_header']]],
  ['numberofrelocations_2',['NumberOfRelocations',['../struct_section__header.html#a14662eb7233065c3c749703cbb494c19',1,'Section_header']]],
  ['numberofrvaandsizes_3',['NumberOfRvaAndSizes',['../struct_optional__header__32__plus.html#a359f7938730e120627d43a79e7f61ad1',1,'Optional_header_32_plus::NumberOfRvaAndSizes()'],['../struct_optional__header__32.html#a8585f76a46bf9c8b0099b63e227c56f2',1,'Optional_header_32::NumberOfRvaAndSizes()']]],
  ['numberofsections_4',['NumberOfSections',['../struct_c_o_f_f__file__header.html#af02626c60549ae92d3f23d489cf4eda3',1,'COFF_file_header']]],
  ['numberofsymbols_5',['NumberOfSymbols',['../struct_c_o_f_f__file__header.html#a0179ff6ae7d234f11861dce892d1f409',1,'COFF_file_header']]]
];
