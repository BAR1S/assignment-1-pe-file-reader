var struct_c_o_f_f__file__header =
[
    [ "Characteristics", "struct_c_o_f_f__file__header.html#ad129b9cf04deb14cdd0cde2a32706deb", null ],
    [ "Machine", "struct_c_o_f_f__file__header.html#ad7cd1d7ebe1c96c49186f15143f94fa3", null ],
    [ "NumberOfSections", "struct_c_o_f_f__file__header.html#af02626c60549ae92d3f23d489cf4eda3", null ],
    [ "NumberOfSymbols", "struct_c_o_f_f__file__header.html#a0179ff6ae7d234f11861dce892d1f409", null ],
    [ "PointerToSymbolTable", "struct_c_o_f_f__file__header.html#aeba29bda97bbb865445ff626d74130a2", null ],
    [ "SizeOfOptionalHeader", "struct_c_o_f_f__file__header.html#a59b559be1c3077ad1f80732b0997fef9", null ],
    [ "TimeDateStamp", "struct_c_o_f_f__file__header.html#a03f3c9a148f465383194a5fd695e3b80", null ]
];