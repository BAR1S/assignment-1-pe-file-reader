var struct_d_o_s__header =
[
    [ "e_cblp", "struct_d_o_s__header.html#a51feb206276a160aff4902600ed15be0", null ],
    [ "e_cp", "struct_d_o_s__header.html#a195ea5d14d935ca398d23ff2de1df27c", null ],
    [ "e_cparhdr", "struct_d_o_s__header.html#ab08e67bc3c39c375f6632e0d33124008", null ],
    [ "e_crlc", "struct_d_o_s__header.html#a3192a343e0a593f023496ac023f72a61", null ],
    [ "e_cs", "struct_d_o_s__header.html#aa1e420c87c7b77fa052431562d9a49ac", null ],
    [ "e_csum", "struct_d_o_s__header.html#a21e044ce7a1fc988d96fe59a5f34b26a", null ],
    [ "e_ip", "struct_d_o_s__header.html#af4f8749c9c1db0e3c79acb3592fcc8bd", null ],
    [ "e_lfanew", "struct_d_o_s__header.html#acf5c95779241eba8f1f4f448d4845036", null ],
    [ "e_lfarlc", "struct_d_o_s__header.html#ad98310c0222da64602709d57f4b544e4", null ],
    [ "e_magic", "struct_d_o_s__header.html#a8aab1155263f7a63e6abc92dff4c8ab9", null ],
    [ "e_maxalloc", "struct_d_o_s__header.html#a12505cfc14eb86f0b714251cb693701c", null ],
    [ "e_minalloc", "struct_d_o_s__header.html#a927c623029e49dc66e772f79f054f3e3", null ],
    [ "e_oemid", "struct_d_o_s__header.html#afd5c1aafac1aecbdb4b8de9b8563bd05", null ],
    [ "e_oeminfo", "struct_d_o_s__header.html#ad8b157f7a0e0394a129c76212b7c00bc", null ],
    [ "e_ovno", "struct_d_o_s__header.html#aabcfec385d010dcb10c8c9b64b7ef6a8", null ],
    [ "e_res", "struct_d_o_s__header.html#a05a5cd511f52b7c7d6dc1f05b4054366", null ],
    [ "e_res2", "struct_d_o_s__header.html#a0ba2674b20af0d1c6a3acb04e9c13a99", null ],
    [ "e_sp", "struct_d_o_s__header.html#ad2e01167268cde941bc4de33134e86db", null ],
    [ "e_ss", "struct_d_o_s__header.html#abd8458b1a3bfa02c2b052745f700409b", null ]
];