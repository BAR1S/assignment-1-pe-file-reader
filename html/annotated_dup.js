var annotated_dup =
[
    [ "COFF_file_header", "struct_c_o_f_f__file__header.html", "struct_c_o_f_f__file__header" ],
    [ "DOS_header", "struct_d_o_s__header.html", "struct_d_o_s__header" ],
    [ "image_data_directory", "structimage__data__directory.html", "structimage__data__directory" ],
    [ "Optional_header", "union_optional__header.html", "union_optional__header" ],
    [ "Optional_header_32", "struct_optional__header__32.html", "struct_optional__header__32" ],
    [ "Optional_header_32_plus", "struct_optional__header__32__plus.html", "struct_optional__header__32__plus" ],
    [ "packed_DOS_header", "structpacked___d_o_s__header.html", "structpacked___d_o_s__header" ],
    [ "packed_PE_header", "structpacked___p_e__header.html", "structpacked___p_e__header" ],
    [ "PE_header", "struct_p_e__header.html", "struct_p_e__header" ],
    [ "Section_header", "struct_section__header.html", "struct_section__header" ]
];